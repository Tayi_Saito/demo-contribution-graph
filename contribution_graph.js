function dateDiffInDays(a, b) {
    // source: https://stackoverflow.com/a/15289883

    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


const graph_class_selector = "contribution-graph";
const locale = "ru-RU";


class ContributionGraph_Day {
    constructor(day_offset) {
        this.day_offset = day_offset;
        this.contrib_num = 0;
        this.$block = null;
    }

    get css_contrib_num_class() {
        const css_class_prefix = "contribution-graph_day_contrib-";
        if (this.contrib_num === 0) {
            return css_class_prefix + '0';
        }
        else if (this.contrib_num < 10) {
            return css_class_prefix + '1-plus';
        }
        else if (10 <= this.contrib_num && this.contrib_num < 30) {
            const category = Math.ceil(this.contrib_num / 10);
            return css_class_prefix + category.toString() + '0-plus';
        }
        else {
            return css_class_prefix + '30-plus';
        }
    }

    set_day_block($block) {
        this.$block = $block;
    }

    set_contrib_num(contrib_num) {
        this.$block.removeClass(this.css_contrib_num_class);
        this.contrib_num = contrib_num;
        this.$block.addClass(this.css_contrib_num_class);
    }

    deselect_block() {
        this.$block.removeClass("contribution-graph_day_selected");
    }

    select_block(date) {
        this.$block.addClass("contribution-graph_day_selected");
    }
}


class ContributionGraph_Modal{
    constructor() {
        this.$block = $('<div>', {
            'class': "contribution-graph_modal"
        });
        this.$block.append($('<div>', {
            'class': "contribution-graph_modal_pointer-head"
        }));
        this.$block.append($('<div>', {
            'class': "contribution-graph_modal_pointer-foot"
        }));
        this.$block.append($('<div>', {
            'class': "contribution-graph_modal_contrib-num"
        }));
        this.$block.append($('<div>', {
            'class': "contribution-graph_modal_date"
        }));
    }

    hide() {
        this.$block.css('visibility', 'hidden');
    }

    show_at($rel, contrib_num, date) {
        this.$block.css('visibility', 'visible');

        if (contrib_num === 0) {
            contrib_num = "No";
        }
        this.$block.find(".contribution-graph_modal_contrib-num").text(
            `${contrib_num} contributions`
        );
        let formatted_date = capitalizeFirstLetter(
            date.toLocaleString(locale, {weekday: 'long'})
        );
        formatted_date += ', ' + capitalizeFirstLetter(
            date.toLocaleString(locale, {month: 'long'})
        );
        formatted_date += ` ${date.getDate()}, ${date.getFullYear()}`;
        this.$block.find(".contribution-graph_modal_date").text(formatted_date);

        const block_width_halved = Math.round(this.$block.outerWidth() / 2);
        const pointer_left = block_width_halved - 5;
        this.$block.find(".contribution-graph_modal_pointer-foot").css('left', pointer_left);

        const block_left = $rel.offset().left + Math.round($rel.width() / 2) - block_width_halved;
        this.$block.css('left', block_left);

        const block_height = Math.round(this.$block.outerHeight());
        this.$block.css('top', $rel.offset().top - block_height - 6);
    }
}


class ContributionGraph {
    constructor(json_url) {
        this.json_url = json_url;
        this.days = [];
        this.graph_json = {};
        this.$graph = $('<div>', {
            'class': graph_class_selector
        });
        this.modal = new ContributionGraph_Modal();
        this.$graph.append(this.modal.$block);
        this.current_date = null;
        this.selected_day = -1;
        this.initData(357);
        this.createGraph();
    }

    initData(max_days) {
        for (let i = this.days.length; i < max_days; i += 1) {
            this.days.push(new ContributionGraph_Day(i));
        }
        if (max_days < this.days.length) {
            this.days = this.days.splice(max_days);
        }
    }

    createGraph() {
        const $data_container = $('<table>', {
            'class': "contribution-graph_data-container"
        });
        const $legend = $('<div>', {
            'class': "contribution-graph_legend"
        });
        this.$graph.append($data_container);

        $legend.append($('<span>', {
            'text': "Меньше",
            'style': "margin-right: 10px"
        }));
        $legend.append($('<div>', {
            'class': "contribution-graph_day"
        }));
        $legend.append($('<div>', {
            'class': "contribution-graph_day contribution-graph_day_contrib-1-plus"
        }));
        $legend.append($('<div>', {
            'class': "contribution-graph_day contribution-graph_day_contrib-10-plus"
        }));
        $legend.append($('<div>', {
            'class': "contribution-graph_day contribution-graph_day_contrib-20-plus"
        }));
        $legend.append($('<div>', {
            'class': "contribution-graph_day contribution-graph_day_contrib-30-plus"
        }));
        $legend.append($('<span>', {
            'text': "Больше",
            'style': "margin-left: 10px"
        }));

        for (let day_num = 0; day_num <= 6; day_num += 1) {
            const $day_row = $('<tr>', {
                'class': "contribution-graph_day-row"
            });
            for (let week_num = 0; week_num < Math.ceil(this.days.length / 7); week_num += 1) {
                const $day = $("<td>", {
                    'class': "contribution-graph_day"
                });
                $day_row.prepend($day);

                const day_offset = week_num * 7 + day_num;
                $day.on('click', this.on_day_click.bind(this, day_offset));
                if (day_offset < this.days.length) {
                    this.days[day_offset].set_day_block($day);
                }
            }
            $data_container.prepend($day_row);

            const $day_of_week = $('<td>', {
                'class': "contribution-graph_day-of-week"
            });
            $day_row.prepend($day_of_week);
        }

        const $month_header = $('<tr>', {
            'class': "contribution-graph_month-row"
        });
        $month_header.append($('<td>', {
            'style': 'width: 0'
        }));
        $data_container.prepend($month_header);

        const $visual_row = $('<tr>');
        $visual_row.append($('<td>'));
        const $legend_td = $('<td>', {
            'colspan': Math.ceil(this.days.length / 7)
        });
        $legend_td.append($legend);
        $visual_row.append($legend_td);
        $data_container.append($visual_row);
    }

    sendRefreshRequest() {
        $.getJSON(this.json_url, this.refreshRequestSuccess.bind(this));
    }

    refreshRequestSuccess(json) {
        this.graph_json = json;
        this.refreshGraph();
    }

    refreshGraph() {
        for (const day_object of this.days) {
            day_object.set_contrib_num(0);
        }

        this.current_date = new Date();
        for (const day in this.graph_json) {
            const contrib_num = this.graph_json[day];
            const date = new Date(day);
            const day_offset = dateDiffInDays(date, this.current_date);
            if (day_offset < this.days.length) {
                this.days[day_offset].set_contrib_num(contrib_num);
            }
        }

        this.redrawGraphAfterRefreshing();
    }

    redrawGraphAfterRefreshing() {
        const $weekdays = this.$graph.find('td.contribution-graph_day-of-week');

        for (let i in $weekdays) {
            if (!$weekdays.hasOwnProperty(i) || isNaN(parseInt(i))) {
                continue;
            }
            i = parseInt(i);
            const $day_of_week = $($weekdays[$weekdays.length - i - 1]);

            const date = new Date();
            date.setDate(this.current_date.getDate() - i);
            const day = date.getDay();
            if (1 <= day && day <= 5 && day % 2 === 1) {
                $day_of_week.css('visibility', 'visible');
            }
            else {
                $day_of_week.css('visibility', 'hidden');
            }

            const text = date.toLocaleDateString(locale, { weekday: 'short' });
            $day_of_week.text(capitalizeFirstLetter(text));
        }

        const $table_header = this.$graph.find(".contribution-graph_month-row");
        $table_header.children(".contribution-graph_month-cell").remove();

        const first_top_row_offset = this.days.length - this.days.length % 7 - 1;
        // if (first_top_row_offset < this.days.length - 1) {
        // }
        let month = null;
        let month_stack = 0;
        for (let day_offset = this.days.length - 1; day_offset > 0; day_offset -= 7) {
            const date = new Date();
            date.setDate(this.current_date.getDate() - day_offset);
            const current_month = date.toLocaleDateString(locale, { month: 'short' });
            if (month === null) {
                month = current_month;
                if (day_offset !== first_top_row_offset) {
                    day_offset = first_top_row_offset + 7;
                    month_stack += 1;
                    continue;
                }
            }
            if (current_month !== month || day_offset === 6) {
                if (day_offset === 6) {
                    month_stack += 1;
                }
                if (month_stack > 2) {
                    const $month_td = $('<td>', {
                        'class': "contribution-graph_month-cell",
                        'colspan': month_stack,
                        'text': capitalizeFirstLetter(month)
                    });
                    $table_header.append($month_td);
                }
                else {
                    $table_header.append($('<td>', {
                        'class': "contribution-graph_month-cell",
                        'colspan': month_stack,
                    }));
                }
                month = current_month;
                month_stack = 0;
            }
            month_stack += 1;
        }
    }

    on_day_click(day_offset, event) {
        if (this.selected_day >= 0) {
            this.days[this.selected_day].deselect_block();
        }
        if (day_offset === this.selected_day || !(day_offset < this.days.length)) {
            this.selected_day = -1;
            this.modal.hide();
            return;
        }
        this.selected_day = day_offset;
        const date = new Date();
        date.setDate(this.current_date.getDate() - day_offset);
        const day = this.days[day_offset];
        day.select_block(date);

        this.modal.show_at(day.$block, day.contrib_num, date);
    }

    showIn(container_selector) {
        this.$graph.detach();
        this.sendRefreshRequest();
        const $container = $(container_selector);
        $container.append(this.$graph);
    }
}